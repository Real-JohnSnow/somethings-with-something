#!/usr/bin/env python

import numpy as np

a1 = -3
a2 = 0
b1 = -1
b2 = 1
Nr = 300
Ni = 300
U1=1.0
E0=1.5

x = np.linspace(a1,a2,Nr,endpoint=True)
y = np.linspace(b1,b2,Ni,endpoint=True)

fname="amp-complex.dat"
with open(fname,"w") as f:
    for i in range(Nr):
        for j in range(Ni):
            E = x[i]+1j*y[j]
            w = U1/(U1+1j*np.sqrt(E0/E))
            f.write(f"{x[i]}\t{y[j]}\t{np.real(w)}\t{np.imag(w)}\t{np.angle(w)}\n")
